import React, { useState, useEffect } from "react";
import "./App.scss";
import {
  TopBannerGrid,
  GreetingHeader,
  WeatherCard,
  WeatherCards,
  WhatToPack,
  JointVentureCredits,
  CreatedBy,
  Modal,
} from "./components";

function App() {
  const [isOpenModal, setOpenModal] = useState(false);
  const [WeatherData, setWeatherData] = useState([]);
  const [modalMessage, setModalMessage] = useState("");

  // on works
  function processWeatherData(data) {
    let groupedByDay = [];

    data.list.forEach((element) => {
      const key = element.dt_txt.split(" ")[0];
      if (groupedByDay[key] === undefined) {
        groupedByDay[key] = [];
      }
      groupedByDay[key].push(element);
    });

    let groupedTemperature = [];
    for (var el in groupedByDay) {
      let max = -1000;
      let min = 1000;
      let description = "";
      let main = "";

      groupedByDay[el].forEach(function (elementByHour) {
        if (description === "")
          description = elementByHour.weather[0].description;
        if (main === "") main = elementByHour.weather[0].main;

        if (elementByHour.main.temp_max > max) {
          max = elementByHour.main.temp_max;
        }

        if (elementByHour.main.temp_min < min) {
          min = elementByHour.main.temp_min;
        }
      });

      groupedTemperature.push({
        date: el,
        max: Math.round(max),
        min: Math.round(min),
        description,
        main,
      });
    }
    return groupedTemperature;
  }

  function ClothingMessage() {
    let countRain = 0;
    let countSun = 0;

    WeatherData.forEach((element) => {
      if (
        element.main === "Rain" ||
        element.main === "Thunderstorm" ||
        element.main === "Clouds"
      ) {
        countRain++;
      } else if (element.main === "Clear") {
        countSun++;
      }
    });

    if (countRain > countSun) {
      return "You should pack warm clothing";
    }
    if (countRain < countSun) {
      return "You can pack light for sunny weather";
    }
    return "Weather is unpredictable, can be as rainy as sunny. Better pack warm and light clothing";
  }

  const api_key = "8c5bc5358429e22c4203a89590ebe3ac";
  const local = "Paris,fr";

  async function getWeatherData() {
    await fetch(
      `https://api.openweathermap.org/data/2.5/forecast?q=${local}&units=metric&APPID=${api_key}`
    )
      .then((response) => response.json())
      .then((data) => {
        const WeatherData = processWeatherData(data);
        setWeatherData(WeatherData);
      });
  }

  useEffect(() => {
    getWeatherData();
  }, []);

  useEffect(() => {
    const modalMessage = ClothingMessage();
    setModalMessage(modalMessage);
  }, [WeatherData]);

  return (
    <>
      <div className="wrapper-container">
        <TopBannerGrid>
          <img
            src="https://images.unsplash.com/photo-1535764558463-30f3af596bee?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80"
            alt="disney-image-1"
          />
          <img
            src="https://images.unsplash.com/photo-1621354598022-16599af1b8b2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
            alt="disney-image-2"
          />
          <img
            src="https://images.unsplash.com/photo-1556950961-8c092986258e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=464&q=80"
            alt="disney-image-3"
          />
        </TopBannerGrid>

        <GreetingHeader>
          <div>
            <h5>Disney Land Paris 2022</h5>
            <h1>Here we go!</h1>
            <p>January 3rd — January 8th</p>
          </div>
          <div>
            <img src="plane.png" />
          </div>
        </GreetingHeader>
      </div>
      <WeatherCards>
        <div>
          <WeatherCard
            date={WeatherData[0]?.date}
            description={WeatherData[0]?.description}
            min={WeatherData[0]?.min}
            max={WeatherData[0]?.max}
            main={WeatherData[0]?.main}
          />
          <WeatherCard
            date={WeatherData[1]?.date}
            description={WeatherData[1]?.description}
            min={WeatherData[1]?.min}
            max={WeatherData[1]?.max}
            main={WeatherData[1]?.main}
          />
          <WeatherCard
            date={WeatherData[2]?.date}
            description={WeatherData[2]?.description}
            min={WeatherData[2]?.min}
            max={WeatherData[2]?.max}
            main={WeatherData[2]?.main}
          />
          <WeatherCard
            date={WeatherData[3]?.date}
            description={WeatherData[3]?.description}
            min={WeatherData[3]?.min}
            max={WeatherData[3]?.max}
            main={WeatherData[3]?.main}
          />
          <WeatherCard
            date={WeatherData[4]?.date}
            description={WeatherData[4]?.description}
            min={WeatherData[4]?.min}
            max={WeatherData[4]?.max}
            main={WeatherData[4]?.main}
          />
        </div>
      </WeatherCards>
      <WhatToPack setOpenModal={setOpenModal} />
      <div className="wrapper-container">
        <JointVentureCredits>
          Celebrating a joint venture: American Tourister and Disneyland Paris
        </JointVentureCredits>
        <CreatedBy />
      </div>

      {isOpenModal && (
        <Modal setOpenModal={setOpenModal} message={modalMessage} />
      )}
    </>
  );
}

export default App;
