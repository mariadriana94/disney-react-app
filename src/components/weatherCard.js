import React from "react";
import styled from "styled-components";

const Card = styled.div`
  position: relative;
  background-color: red;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 250px;
  padding: 20px;
  border-radius: 10px;
  cursor: pointer;
  transition: all 2s ease-in-out;
  img {
    position: absolute;
    top: -80px;
  }
  span {
    color: var(--white-07);
    text-align: center;
  }
`;

const Info = styled.div`
  display: inherit;
  flex-direction: column;
  align-items: center;
  div.temperature {
    display: inherit;
    flex-direction: row;
    gap:10px;
    justify-content: center;
    align-items: baseline;
    h3 {
      color: var(--white);
      font-size: 3.5rem;
    }
    h5 {
      color: var(--white-07);
      font-size: 1rem;
    }
  }
`;

export function WeatherCard({ description, min, max, main, date }) {
  let weatherImg = "";
  let backgroundGradient = "";

  switch (main) {
    case "Clear":
      weatherImg = "sunny-chance-of-clouds.png";
      backgroundGradient = "linear-gradient(25deg, #2FD9E4 0%, #EFD74D 100%)";
      break;
    case "Clouds":
      weatherImg = "sunny-cloudy.png";
      backgroundGradient =
        "linear-gradient(25deg, #6F7E89 0%,  #35CDD7 50%,#3BEFFB 100%)";
      break;
    case "Drizzle":
    case "Rain":
      weatherImg = "sunny-rainy.png";
      backgroundGradient = "linear-gradient(25deg, #3F4E5F 0%, #27B2BB 100%)";
      break;
    case "Thunderstorm":
      weatherImg = "sunny-stormy.png";
      backgroundGradient =
        "linear-gradient(25deg, var(--blue) 0%, var(--green) 100%)";
      break;
    case "Snow":
      weatherImg = "sunny-cloudy.png";
      backgroundGradient =
        "linear-gradient(25deg, var(--blue) 0%, var(--green) 100%)";
      break;
    case "Atmosphere":
      weatherImg = "sunny-cloudy.png";
      backgroundGradient =
        "linear-gradient(25deg, var(--blue) 0%, var(--green) 100%)";
      break;
    default:
      weatherImg = "sunny-cloudy.png";
      backgroundGradient =
        "linear-gradient(25deg, var(--blue) 0%, var(--green) 100%)";
      backgroundGradient =
        "linear-gradient(25deg, var(--blue) 0%, var(--green) 100%)";
      break;
  }

  return (
    <Card style={{ background: backgroundGradient }}>
      <img src={weatherImg} alt="Weather Icon" />
      <Info>
        <div class="temperature">
          <h3>{max}º</h3>
          <h5>{min}º</h5>
        </div>
        <span>{description}</span>
        <span>{date}</span>
      </Info>
    </Card>
  );
}
