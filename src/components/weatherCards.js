import styled from "styled-components";

export const WeatherCards = styled.section`
  position: relative;
  margin: 150px 0px;
  &:before {
    position: absolute;
    content: "";
    width: 100%;
    height: 100%;
    bottom: -50%;
    border-top-left-radius: 100%;
    border-top-right-radius: 100%;
    background: linear-gradient(
      180deg,
      #c4c4c4 0%,
      rgba(196, 196, 196, 0) 100.09%
    );
    opacity: 0.5;
  }
  > div {
    max-width: 1050px;
    margin: 0 auto;
    display: grid;
    grid-gap: 40px;
    grid-template-columns: repeat(5, 1fr);
  }

  /* mobile */
  @media (max-width: 768px) {
    margin: 0 20px;
    &:before {
     display:none
    }
    > div {
      grid-template-columns: 1fr;
      grid-gap: 150px;
    }
  }
`;
