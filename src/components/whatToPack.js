import React from "react";
import styled from "styled-components";

const Banner = styled.section`
  display: grid;
  grid-template-columns: 3fr 1fr;
  max-width: 1050px;
  margin: 0 auto;
  padding: 80px 0;
  position: relative;
  h5,
  h6 {
    font-family: "Open Sans", sans-serif;
    font-size: 2rem;
  }
  h6 {
    font-weight: 500;
  }
  h5 {
    font-weight: 800;
  }

  /* mobile */
  @media (max-width: 768px) {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    > div {
      margin: 50px 0;
      text-align: center;
    }
  }
`;

const Button = styled.button`
  background-image: url("btn_bg.png");
  background-color: transparent;
  background-position: center;
  background-size: contain;
  background-repeat: no-repeat;
  height: 75px;
  width: 300px;
  border: 0;
  font-family: "Gloria Hallelujah", cursive;
  color: var(--white);
  font-size: 1.2rem;
  padding: 0 0 20px 0;
`;

export function WhatToPack({ setOpenModal }) {
  return (
    <Banner onClick={() => setOpenModal(true)}>
      <div>
        <h6>Not sure on what to pack?</h6>
        <h5>Let us help you with that!</h5>
      </div>
      <Button>What should I pack?</Button>
    </Banner>
  );
}
