import React from "react";

import styled from "styled-components";

const Backdrop = styled.div`
  position: fixed;
  top: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const Container = styled.div`
  position: relative;
  height: 600px;
  min-width: 1050px;
  background-color: #dcf0ee;
  display: grid;
  grid-template-columns: 1fr 1fr;
  padding: 40px;
  border-radius: 10px;
  div {
    display: flex;
    flex-direction: column;
    justify-content: center;
    &:nth-child(2) {
      align-items: center;
    }
    h5 {
      text-transform: uppercase;
      font-family: "Open Sans", sans-serif;
    }
    h1 {
      font-family: "Gloria Hallelujah", cursive;
    }
    p {
      font-family: "Open Sans", sans-serif;
    }
    img {
      max-width: 350px;
      mix-blend-mode: darken;
    }
  }

  /* mobile */
  @media (max-width: 768px) {
    width: 90%;
    min-width: unset;
    height: 90%;
    grid-template-columns: 1fr;
    position: absolute;
    top: 5%;
    left: 5%;
  }
`;

const Close = styled.span`
  position: absolute;
  top: 50px;
  right: 50px;
  width: 40px;
  height: 40px;
  background-color: rgba(255, 255, 255, 0.6);
  border-radius: 100%;
  cursor: pointer;
  &:before {
    content: "";
    position: absolute;
    height: 1px;
    width: 20px;
    background-color: var(--grey-800);
    transform: rotate(-45deg);
    top: 20px;
    left: 10px;
    transition: 0.3s all ease-in-out;
  }
  &:after {
    content: "";
    position: absolute;
    height: 1px;
    width: 20px;
    background-color: var(--grey-800);
    transform: rotate(45deg);
    top: 20px;
    left: 10px;
    transition: 0.3s all ease-in-out;
  }

  &:hover {
    &:before,
    &:after {
      transform: rotate(0deg);
    }
  }

  /* mobile */
  @media (max-width: 768px) {
    top: 20px;
  right: 20px;
  }
`;

export function Modal({ message, setOpenModal }) {
  function closeModal() {
    setOpenModal(false);
  }

  return (
    <Backdrop>
      <Container>
        <div>
          <h5>pack it up</h5>
          <h1>This is what you’ll need to bring</h1>
          <p>{message}</p>
        </div>
        <div>
          <img
            src="travelbag.png"
            alt="Travel Bag"
            aria-label="Travel bag image"
          />
        </div>
        <Close onClick={closeModal} />
      </Container>
    </Backdrop>
  );
}
