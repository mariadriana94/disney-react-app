import React from "react";
import styled from "styled-components";

const Credits = styled.section`
  margin: 50px 0;
  position: relative;
`;

export function CreatedBy() {
  return (
    <Credits>
      <a href="www.maria-adriana.com" target="_blank" aria-label="portfolio">
        Maria Adriana
      </a>
    </Credits>
  );
}
