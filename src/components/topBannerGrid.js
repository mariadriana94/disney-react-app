import styled from "styled-components";

export const TopBannerGrid = styled.section`
display: grid;
grid-gap: 40px;
grid-template-columns: 3fr 1fr 1fr;
  img{
    width: 100%;
    object-fit: cover;
    object-position: center;
    border-bottom-left-radius: 5px;
    border-bottom-right-radius: 5px;
    &:nth-child(1),
    &:nth-child(3) {
      height: 150px;
    }
    &:nth-child(2) {
      height: 200px;
    }
  }
  
  /* mobile */
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    padding:0px 20px;
    img:nth-child(2),
    img:nth-child(3) {
      display:none;
    }
  }
`;
