import styled from "styled-components";

export const GreetingHeader = styled.section`
  margin: 120px 0px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 40px;

  div {
    display: flex;
    &:nth-child(1) {
      flex-direction: column;
      justify-content: flex-start;
    }
    &:nth-child(2) {
      flex-direction: row;
    }
    h5 {
      color: var(--grey-800);
      text-transform: uppercase;
      font-size: 2rem;
    }
    h1 {
      color: var(--green);
      font-family: "Gloria Hallelujah", cursive;
      font-size: 3rem;
      line-height: 2rem;
    }
    p {
      color: var(--grey-500);
      font-family: "Open Sans", sans-serif;
    }
    img {
      max-width: 300px;
      right: 0px;
      animation-name: planeFlyDesktop;
      animation-duration: 2s;
      animation-fill-mode: forwards;
    }
  }

  /* mobile */
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    div {
      &:nth-child(1),
      &:nth-child(2) {
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
      img {
        animation-name: planeFlyMobile;
        animation-duration: 2s;
        animation-fill-mode: forwards;
      }
    }
  }

  @keyframes planeFlyDesktop {
    from {
      transform: translateX(0px);
      opacity: 0;
    }
    to {
      transform: translateX(100px);
      opacity: 1;
    }
  }

  @keyframes planeFlyMobile {
    from {
      transform: translateY(0px);
      opacity: 0;
    }
    to {
      transform: translateY(-50px);
      opacity: 1;
    }
  }
`;
